#!/usr/bin/env python

import rospy
from sensor_msgs.msg import PointCloud2
from stereo_msgs.msg import DisparityImage

# from cv_bridge import CvBridge
import ros_numpy


def pc2_callback(data):
    # trisectrospy.loginfo(rospy.get_caller_id() + " I got a point cloud with %d byte" % len(data.data))

    offset_sorted = {f.offset: f for f in data.fields}
    data.fields = [f for (_, f) in sorted(offset_sorted.items())]

    # Conversion from PointCloud2 msg to np array.
    pc_np = ros_numpy.point_cloud2.pointcloud2_to_xyz_array(data, remove_nans=True)
    rospy.loginfo("Point cloud shape: " + str(pc_np.shape))

    # pc_np_sorted = pc_np[pc_np[:, 2].argsort()]

    # print(len(pc_np_sorted))
    # print(pc_np[0])
    # print(pc_np[1])
    # print(pc_np[-2])
    # print(pc_np[-1])


def disparity_callback(data):
    rospy.loginfo("Got disparity")
    rospy.loginfo(" min_disparity: %f" % data.min_disparity)

    # bridge = CvBridge()
    # cv_image = bridge.imgmsg_to_cv2(data.image, desired_encoding="passthrough")
    # print(cv_image[0][0])


def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node("point2_watcher", anonymous=True)

    topic = "/trisect/stereo/points2"
    # topic = "/trisect/stereo/passthrough/output"

    rospy.Subscriber(topic, PointCloud2, pc2_callback)

    rospy.Subscriber("/trisect/stereo/disparity", DisparityImage, disparity_callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


if __name__ == "__main__":
    listener()
