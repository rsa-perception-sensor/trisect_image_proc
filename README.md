Set of ROS nodelets which replicate existing functionality in `stereo_image_proc` and other packages with minor adjustments for Trisect.


# trisect_image_proc::crop_decimate

Based on [image_proc/image_pipeline/crop_decimate], **BUT** the resulting `camera_info` does not use the `binning_*` fields.  Instead, the intrinsics are scaled appropriately.  It also replaces the distortion parameters with zero values.
