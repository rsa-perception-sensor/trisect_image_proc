
// At this point, this code is a slightly feature-reduced (e.g. no params or dynamic reconfigure) 
// copy of image_proc/rectify

#include <mutex>

#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <image_transport/image_transport.h>
#include <image_geometry/pinhole_camera_model.h>
#include <cv_bridge/cv_bridge.h>

namespace trisect_image_proc {


    class RectifyNodelet : public nodelet::Nodelet {
public:

RectifyNodelet() :
queue_size_(10), rectified_frame_suffix_("")
{}

void onInit() override {
    ros::NodeHandle &nh         = getNodeHandle();
    ros::NodeHandle &private_nh = getPrivateNodeHandle();
    it_.reset(new image_transport::ImageTransport(nh));
    
    // Monitor whether anyone is subscribed to the output
    image_transport::SubscriberStatusCallback connect_cb = std::bind(&RectifyNodelet::connectCb, this);
    
    // Make sure we don't enter connectCb() between advertising and assigning to pub_rect_
    std::lock_guard<std::mutex> lock(connect_mutex_);

    pub_rect_  = it_->advertise("image_rect",  1, connect_cb, connect_cb);
}

// Handles (un)subscribing when clients (un)subscribe
void connectCb()
{
  std::lock_guard<std::mutex> lock(connect_mutex_);
  if (pub_rect_.getNumSubscribers() == 0)
    sub_camera_.shutdown();
  else if (!sub_camera_)
  {
    image_transport::TransportHints hints("raw", ros::TransportHints(), getPrivateNodeHandle());
    sub_camera_ = it_->subscribeCamera("image_raw", queue_size_, &RectifyNodelet::imageCb, this, hints);
  }
}

void imageCb( const sensor_msgs::ImageConstPtr &image_msg, const sensor_msgs::CameraInfoConstPtr &info_msg ){
  // Verify camera is actually calibrated
  if (info_msg->K[0] == 0.0) {
    NODELET_ERROR_THROTTLE(30, "Rectified topic '%s' requested but camera publishing '%s' "
                           "is uncalibrated", pub_rect_.getTopic().c_str(),
                           sub_camera_.getInfoTopic().c_str());
    return;
  }

  // If zero distortion, just pass the message along
  bool zero_distortion = true;
  for (size_t i = 0; i < info_msg->D.size(); ++i)
  {
    if (info_msg->D[i] != 0.0)
    {
      zero_distortion = false;
      break;
    }
  }
  // This will be true if D is empty/zero sized
  if (zero_distortion)
  {
    pub_rect_.publish(image_msg);
    return;
  }

  // Update the camera model
  model_.fromCameraInfo(info_msg);
  
  // Create cv::Mat views onto both buffers
  const cv::Mat image = cv_bridge::toCvShare(image_msg)->image;
  cv::Mat rect;

  // Rectify and publish
  int interpolation;
  {
    std::lock_guard<std::recursive_mutex> lock(config_mutex_);
    interpolation = 0;
  }
  model_.rectifyImage(image, rect, interpolation);

  // Allocate new rectified image message
  sensor_msgs::ImagePtr rect_msg = cv_bridge::CvImage(image_msg->header, image_msg->encoding, rect).toImageMsg();
  
  // If 'rectified_frame_suffix_' is not empty, add to header
  if (!rectified_frame_suffix_.empty())
  {
    rect_msg->header.frame_id += rectified_frame_suffix_;
  }
  
  pub_rect_.publish(rect_msg);
}

private:

int queue_size_;
std::string rectified_frame_suffix_;

  // Processing state (note: only safe because we're using single-threaded NodeHandle!)
  image_geometry::PinholeCameraModel model_;

std::mutex connect_mutex_;
image_transport::Publisher pub_rect_;

std::recursive_mutex config_mutex_;

std::shared_ptr<image_transport::ImageTransport> it_;
image_transport::CameraSubscriber sub_camera_;

    };


} // namespace trisect_camera_driver

// Register nodelet
#include <pluginlib/class_list_macros.hpp>
PLUGINLIB_EXPORT_CLASS( trisect_image_proc::RectifyNodelet, nodelet::Nodelet)
