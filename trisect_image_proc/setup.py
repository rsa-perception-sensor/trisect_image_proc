from setuptools import setup

setup(
    name="trisect_image_proc",
    version="0.1.0",
    # packages=["trisect_image_proc"],
    # package_dir={"": "src"},
    install_requires=[
        "sensor_msgs",
    ],
    scripts=["scripts/image_difference"],
)
